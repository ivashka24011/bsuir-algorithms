#include <iostream>
#include <vector>
#include <chrono>
#include <cstdlib>

void insertionSort(std::vector<int>& arr) {
    int size = arr.size();
    for (int i = 1; i < size; i++) {
        int key = arr[i];
        int j = i - 1;

        while (j >= 0 && arr[j] > key) {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
    }
}

void selectionSort(std::vector<int>& arr) {
    int size = arr.size();
    for (int i = 0; i < size - 1; i++) {
        int min_index = i;
        for (int j = i + 1; j < size; j++) {
            if (arr[j] < arr[min_index]) {
                min_index = j;
            }
        }
        std::swap(arr[min_index], arr[i]);
    }
}

void printArray(std::vector<int>& arr) {
    for (int i = 0; i < arr.size(); i++) {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
}

int main() {
    int size = 100000; // задаем размер вектора
    std::vector<int> arr(size), arr2(size);

    // Заполняем вектор случайными числами
    for (int i = 0; i < size; i++) {
        int num = rand() % 100000; // генерируем случайное число от 0 до 99
        arr[i] = num;
        arr2[i] = num;
    }

    auto start = std::chrono::high_resolution_clock::now();

    insertionSort(arr);

    auto end = std::chrono::high_resolution_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
    std::cout << "Time taken by insertion sort: "
              << duration.count() << " microseconds" << std::endl;

    start = std::chrono::high_resolution_clock::now();

    selectionSort(arr2);

    end = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
    std::cout << "Time taken by selection sort: "
              << duration.count() << " microseconds" << std::endl;

    return 0;
}
