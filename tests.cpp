#include <gtest/gtest.h>
#include <vector>
#include <algorithm>

extern void insertionSort(std::vector<int>& arr);
extern void selectionSort(std::vector<int>& arr);

TEST(SortTest, InsertionSort) {
    std::vector<int> arr {5, 2, 4, 6, 1, 3};
    insertionSort(arr);
    for(int i = 1; i < arr.size(); i++) {
        ASSERT_LE(arr[i-1], arr[i]);
    }
}

TEST(SortTest, SelectionSort) {
    std::vector<int> arr {5, 2, 4, 6, 1, 3};
    selectionSort(arr);
    for(int i = 1; i < arr.size(); i++) {
        ASSERT_LE(arr[i-1], arr[i]);
    }
}

TEST(SortTest, InsertionSortEmpty) {
    std::vector<int> arr;
    insertionSort(arr);
    ASSERT_TRUE(arr.empty());
}

TEST(SortTest, SelectionSortEmpty) {
    std::vector<int> arr;
    selectionSort(arr);
    ASSERT_TRUE(arr.empty());
}

TEST(SortTest, InsertionSortAlreadySorted) {
    std::vector<int> arr {1, 2, 3, 4, 5, 6};
    insertionSort(arr);
    for(int i = 1; i < arr.size(); i++) {
        ASSERT_LE(arr[i-1], arr[i]);
    }
}

TEST(SortTest, SelectionSortAlreadySorted) {
    std::vector<int> arr {1, 2, 3, 4, 5, 6};
    selectionSort(arr);
    for(int i = 1; i < arr.size(); i++) {
        ASSERT_LE(arr[i-1], arr[i]);
    }
}
